#!/usr/bin/env python3
# get html for 'https://www.last.fm/user/seanXdavis/library'
# using selenium and chromedriver
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import time
# set up selenium
options = Options()
# options.add_argument('--headless')
options.add_argument('--disable-gpu')
options.add_argument('--window-size=1920x1080')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
Options.binary_location = "/nix/store/m8ys3w3amibjq4xn9w5a0z38qwr1k6yz-google-chrome-120.0.6099.199/bin/google-chrome-stable"
driver = webdriver.Chrome(options=options)

def get(url):
    print(f'getting {url}')
    driver.get(url)
    time.sleep(1)
    html = driver.page_source
    return html


if '__main__' == __name__:
    # for each os arg, get html and print 
    for arg in os.sys.argv[1:]:
        print(get(arg))