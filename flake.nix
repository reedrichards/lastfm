{
  description = "A basic gomod2nix flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
          selenium = pkgs.callPackage ./selenium.nix {
            inherit (pkgs) lib fetchFromGitHub;
            buildPythonPackage = pkgs.python3Packages.buildPythonPackage;
            certifi = pkgs.python3Packages.certifi;
            geckodriver = pkgs.geckodriver;
            pytestCheckHook = pkgs.python3Packages.pytestCheckHook;
            trio = pkgs.python3Packages.trio;
            trio-websocket = pkgs.python3Packages.trio-websocket;
            urllib3 = pkgs.python3Packages.urllib3;
            pythonOlder = f: false;
          };

        in
        {
          packages.devContainer = pkgs.dockerTools.buildLayeredImage {
            
          };
          devShells.default = pkgs.mkShell {
            packages = [
              pkgs.chromedriver
              pkgs.python3
              pkgs.awscli2
              pkgs.nixpkgs-fmt
              selenium
              pkgs.google-chrome
              # install bs4
              pkgs.python3Packages.beautifulsoup4
            ];
            shellHook = ''
              python3 -m venv .venv
              source .venv/bin/activate
              export DISPLAY=:0
            '';
          };
        })
    );
}
