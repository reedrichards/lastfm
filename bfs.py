from collections import deque, defaultdict

from get import get

from links import get_links

import os


def get_edges(vertex):
    edges = set()
    all_links = [f'{vertex}/following?page={i}' for i in range(1, 100)]
    for link in all_links:
        html = get(link)
        links = get_links(html)
        len_before = len(edges)
        edges.update(links)
        len_after = len(edges)
        if len_before == len_after:
            break
    return edges


def bfs(graph, start, f, get_edges):
    visited = set()
    queue = deque([start])
    iter = 0
    while queue:
        vertex = queue.popleft()
        if vertex not in visited:
            # get edges from last.fm and add to graph
            graph[vertex].extend(get_edges(vertex))
            # append function to vertex
            f(graph)
            visited.add(vertex)
            queue.extend(neighbor for neighbor in graph[vertex] if neighbor not in visited)

if '__main__' == __name__:
    
    # Example graph represented as an adjacency list
    # graph = {
    #     "https://www.last.fm/user/Andergrammar": [],
    # }

    # # Starting vertex for BFS
    # start_vertex = 'https://www.last.fm/user/Andergrammar'
    graph = defaultdict(list)

    for arg in os.sys.argv[1:]:
        # Perform BFS
        print("BFS starting from vertex", arg)
        bfs(
            graph, 
            arg, 
            print,
            get_edges
            )
