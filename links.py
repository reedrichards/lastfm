#!/usr/bin/env python3
# from out.html
# get a list of links matching
#                                 
# <a href="/user/Andergrammar" class="
#                                 user-list-link
#                                 link-block-target
#                             ">Andergrammar</a>

from bs4 import BeautifulSoup
import json
import os


def get_links(html):
    soup = BeautifulSoup(html, 'html.parser')
    links = soup.find_all('a', class_='user-list-link')
    return [ "https://www.last.fm" + link['href'] for link in links]
