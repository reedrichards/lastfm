#!/usr/bin/env python3
# from out.html
# get table matching 
    # <table class="
    #         chartlist
            # and convert to json

from bs4 import BeautifulSoup
import json
import os 


def get_table(html):
    soup = BeautifulSoup(html, 'html.parser')
    table = soup.find('table', class_='chartlist')
    return table

def get_rows(table):
    rows = table.find_all('tr')
    return rows

def get_row_data(row):
    cells = row.find_all('td')
    data = [cell.text.strip() for cell in cells]
    return data

def get_table_data(table):
    rows = get_rows(table)
    table_data = [get_row_data(row) for row in rows]
    return table_data

def get_table_json(table):
    table_data = get_table_data(table)
    table_json = json.dumps(table_data)
    return table_json

def get_table_json_from_html(html):
    table = get_table(html)
    table_json = get_table_json(table)
    return table_json

def get_table_json_from_file(file):
    html = open(file, 'r').read()
    table_json = get_table_json_from_html(html)
    return table_json

def main():

    print(get_table_json_from_file('out.html'))

if __name__ == '__main__':
    main()